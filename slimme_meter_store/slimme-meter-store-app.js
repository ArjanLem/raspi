#!/usr/bin/env node

var amqp = require('amqplib/callback_api');

const QUEUE_NAME = 'slimme-meter-acq';

amqp.connect('amqp://localhost', function(err, conn) {
  conn.createChannel(function(err, ch) {

    ch.assertQueue( QUEUE_NAME, {durable: false});
    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", QUEUE_NAME);
    ch.consume( QUEUE_NAME, function(msg) {
      	console.log(" [x] Received %s", msg.content.toString());
    	var message = JSON.parse(msg.content.toString())
    	console.log("  message:",message)
    }, {noAck: true});
  });
});