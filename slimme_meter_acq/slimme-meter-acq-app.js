console.log("Start Slimme Meter Acquisition")

// lodash ----------------------------------------
var _ = require('lodash-node');
var moment = require('moment');
moment.locale('nl');

// log4js ----------------------------------------
/*
var log4js = require('log4js');
log4js.configure({
  appenders: [
    { type: 'console' },
    { 
        type: 'file', 
        filename: 'logs/app.log',          
        pattern: '-yyyy-MM-dd',
        category: 'app' 
    }, { 
        type: 'file', 
        filename: 'logs/measurements.csv', 
        maxLogSize: 1024,
        backups: 3,
        category: 'measurements' 
    }
  ]
});

var logger = log4js.getLogger('app');
logger.setLevel('DEBUG');
var logMeas = log4js.getLogger('measurements');
logMeas.setLevel('DEBUG');
*/

var winston = require('winston');
//var expressWinston = require('express-winston');
winston.transports.DailyRotateFile = require('winston-daily-rotate-file');
var tsFormat = () => (new Date()).toLocaleTimeString();

var logger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({
            timestamp: tsFormat,
            colorize: true,
        }),
        new winston.transports.DailyRotateFile({
            name: 'file',
            json: false,
            filename: 'logs/app.log',
            datePattern: '.yyyy-MM-dd'
        })
    ]
});
var logMeas = new (winston.Logger)({
    transports: [
        // new (winston.transports.Console)(),
        new winston.transports.DailyRotateFile({
            name: 'file',
            json: false,
            datePattern: '_yyyy-MM-dd.csv',
            filename: 'logs/measurements',
            formatter: function(options) {
                return options.message;
            }
        })
    ]
});

// MongoDB ----------------------------------------
/*
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');

// Connection URL
var url = 'mongodb://localhost:27017/slimmemeter';

// Use connect method to connect to the server
MongoClient.connect(url, function(err, db) {
  assert.equal(null, err);
  console.log("MongoDb - Connected successfully to server");

  db.close();
});
*/

// RabbitMQ ----------------------------------------var amqp = require('amqplib');
var amqp = require('amqplib/callback_api');
const QUEUE_NAME = 'slimme-meter-acq';

function postQueueMessage(message) {
    logger.info("postQueueMessage() - message:",message )

    amqp.connect('amqp://localhost', function(err, conn) {
        if( err ) { 
            console.error("connect - err:"+err)
        } else { 
            conn.createChannel(function(err, ch) {
                if( err ) {
                    console.error("createChannel - err:"+err)
                } else {
                    ch.assertQueue( QUEUE_NAME, {durable: false});
                    // Note: on Node 6 Buffer.from(msg) should be used
                    var jsonMsg = JSON.stringify(message)
                    ch.sendToQueue( QUEUE_NAME, new Buffer(jsonMsg));
                    logger.info(" [x] Sent messag:", jsonMsg );
                }
            });
        }
    });
}

// start the serial port ----------------------------------------
var SerialPort = require('serialport');
logger.debug("serialport.list")
SerialPort.list(function (err, ports) {
  ports.forEach(function(port) {
    logger.info(port.comName);
    //logger.info('  '+port.pnpId);
    //logger.info('  '+port.manufacturer);
  });
});


logger.info("Opening serialport")
var SerialPort = require("serialport");
var port = new SerialPort("/dev/ttyUSB0", {
    baudRate: 115200,
    parser: SerialPort.parsers.readline('\n')
});

function parseDate(line) {
    // 0-0:1.0.0(161029172852S)
    var dateStr = line.substring(line.lastIndexOf("(")+1,line.lastIndexOf(")"));
    // '161029172852S'
    // http://momentjs.com/docs/
    return moment(dateStr, 'YYMMDDHHmmss').toDate()
}

function parseTarif(line) {
    // 0-0:96.14.0(0001)
    return line.substring(line.lastIndexOf("(")+1,line.lastIndexOf(")"));
}

function parseMeterReading(line) {
    // 1-0:2.8.2(001101.853*kWh)
    var pwrStr = line.substring(line.lastIndexOf("(")+1,line.lastIndexOf("*"));
    return parseFloat(pwrStr)
}

function parsePower(line) {
    // 1-0:1.7.0(00.255*kW)
    var pwrStr = line.substring(line.lastIndexOf("(")+1,line.lastIndexOf("*"));
    return parseFloat(pwrStr)
}

function parseGas(line) {
    // 0-1:24.2.1(161029170000S)(00255.252*m3)
    var pwrStr = line.substring(line.lastIndexOf("(")+1,line.lastIndexOf("*"));
    return parseFloat(pwrStr)
}

/**
 * Number.prototype.format(n, x, s, c)
 * 
 * @param integer n: length of decimal
 * @param integer x: length of whole part
 * @param mixed   s: sections delimiter
 * @param mixed   c: decimal delimiter
 */
Number.prototype.format = function(n, x, s, c) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};

function csvFormatLong(lng) {
    return lng.format(3, 3, '.', ',');
}

const bufferSize = 10;
var lastMeasurementsBuffer = [];
function lastMeasurementsBufferPush(m) {
    lastMeasurementsBuffer.push(m)

    if( lastMeasurementsBuffer.length>bufferSize ) {
        lastMeasurementsBuffer = _.drop(lastMeasurementsBuffer, lastMeasurementsBuffer.length-bufferSize)   
    }
}

function parseSlimmeMeterMessage(messagelines) {
    var message = {}
    var fields = [
        { key:'date',     tag: '0-0:1.0.0',   parser: parseDate },
        { key:'t1_plus',  tag: '1-0:1.8.1',   parser: parseMeterReading },
        { key:'t2_plus',  tag: '1-0:1.8.2',   parser: parseMeterReading },
        { key:'t1_min',   tag: '1-0:2.8.1',   parser: parseMeterReading },
        { key:'t2_min',   tag: '1-0:2.8.2',   parser: parseMeterReading },
        { key:'pow_plus', tag: '1-0:1.7.0',   parser: parsePower },
        { key:'pow_min',  tag: '1-0:2.7.0',   parser: parsePower },
        { key:'tarief',   tag: '0-0:96.14.0', parser: parseTarif },
        { key:'gas',      tag: '0-1:24.2.1',  parser: parseGas },
    ]

    // logger.info('Message');
    _.forEach(messagelines, function(line) {
        _.forEach(fields, function(field) {
            if( line.indexOf(field.tag)>=0 ) {
                // logger.info( field.key + ' -> :'+line );
                message[field.key] = field.parser(line)
            }
        });
    });
    //logger.info('message:', message );

    var msg = 
        moment(message.date).format('YYYY-MM-DD HH:mm:ss') + ';' +
        csvFormatLong(message.t1_plus) + ';' +
        csvFormatLong(message.t1_min) + ';' +
        csvFormatLong(message.t2_plus) + ';' +
        csvFormatLong(message.t2_min) + ';' +
        csvFormatLong(message.pow_plus) + ';' +
        csvFormatLong(message.pow_min) + ';' +
        message.tarief + ';' +
        csvFormatLong(message.gas) ;
    logMeas.info(msg)

    lastMeasurementsBufferPush(message);
    postQueueMessage(message)
}

var mesLines = null

function parseSlimeMeterline(line ) {
    if ( line.length==0 ) return
    // start message
    if( line.indexOf('/')>=0 ) {
        // logger.info('start');
        mesLines = [];
    } else if( mesLines ) {
        // end message
        if( line[0]=='!' ) {
            // logger.info('end');
            parseSlimmeMeterMessage(mesLines);
            mesLines=null;
        } else {
            //logger.info('push  :'+line);
            mesLines.push(line);
        }
    }
}

port.on('data', function (data) {
    // logger.info('Data: ' + data );
    parseSlimeMeterline(data);
});  
 
// open errors will be emitted as an error event 
port.on('error', function(err) {
    logger.error('Error: ', err.message);
})

// Start the server 
const http = require('http');

const hostname = '0.0.0.0';
const httpPort = 3000;

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    msg = 'Last '+ bufferSize + ' Measurements:\n\n'
    _.forEach(lastMeasurementsBuffer, function(m, idx) {
        msg = msg +
            ("0" + (idx+1) ).slice(-2) + ':'+
            ' ' + moment(m.date).format('YYYY/MM/DD-HH:mm:ss') + 
            ' T1+:' + csvFormatLong(m.t1_plus) + 'kWh' +
            ' T1-:' + csvFormatLong(m.t1_min) + 'kWh' +
            ' T2+:' + csvFormatLong(m.t2_plus) + 'kWh' +
            ' T2-:' + csvFormatLong(m.t2_min) + 'kWh' +
            ' Pow+:' + csvFormatLong(m.pow_plus) + 'kW' +
            ' Pow-:' + csvFormatLong(m.pow_min) + 'kW' +
            ' Trf:' + m.tarief +
            ' Gas:' + csvFormatLong(m.gas) + 'm3';
        msg = msg + '\n'
    });

    res.end(msg);
});

server.listen(httpPort, hostname, () => {
    console.info(`Server running at http://${hostname}:${port}/`);
});
